import React, {useState} from 'react';
import { toJS } from 'mobx';
import {
  Grid,
  TextField,
  Button
} from '@material-ui/core';
import Swal from 'sweetalert2';
import Layout from '../../component/Layout'
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router'


const Page = (props) => {
  const { loginStatus } = toJS(props.user)
  if(loginStatus) props.history.push(`/todo`)
  const [userName, setUserName] = useState('')
  const [password, setPassword] = useState('')

  const validation = () => {
    return userName && password
  }

  const login = async () => {
    const inputValid = validation()
    if(!inputValid) {
      Swal.fire('username or password is empty')
      return
    }
    const status = await props.user.login(userName, password)
    if(status) props.history.push(`/todo`)
  }


  return (
    <Layout>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          required
          fullWidth
          id="user"
          label="User Name"
          name="user"
          autoComplete="user"
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Grid>
      <Button variant="outlined" color="primary" onClick={() => login()}>
        Login
      </Button>
    </Layout>
  )
}

const pageRouter = withRouter(Page)
export default inject('user')(observer(pageRouter))
