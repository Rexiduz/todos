import { extendObservable, toJS } from 'mobx';
import axios from 'axios';

export class Bonus {
  constructor() {
    extendObservable(this, {
      loading: false
    });
  }

//   โบนัส 
// กำหนดข้อมูล 2 ชุด เป็น array of number จงคิดวิธีการในการ filter array ชุดแรก ให้เหลือเพียงแค่สมาชิกที่มีใน array ที่ชุดสอง
  bonus(){
    const arrFirst = [1,2,3,4,5,6,7,8,9,0]
    const arrSecond = [4,8,2]
    const newArr = arrFirst.filter(no => arrSecond.indexOf(no) !== -1)
    console.log(newArr)
  }

}

const bonus = new Bonus();
export default bonus;
