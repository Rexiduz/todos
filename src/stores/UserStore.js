import { extendObservable, toJS } from 'mobx';
import axios from 'axios';

export class User {
  constructor() {
    extendObservable(this, {
      loginStatus: false,
      token: ''
    });
  }

  async login(username, password){
    try{
      const res = await axios.post(`https://candidate.neversitup.com/todo/users/auth`, {
        username, 
        password
      })
      if(res.status === 200){
        this.loginStatus = true
        this.token = res.data.token
        return this.loginStatus
      } 
    }catch(e){
      console.log(e)
    }
  }

}

const user = new User();
export default user;
